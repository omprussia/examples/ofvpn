# SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TEMPLATE = lib
TARGET = ofvpnplugin
QT += qml dbus
QT -= gui
CONFIG += plugin c++14

HEADERS += \
    ofvpnconfiguratoradaptor.h \
    ofvpnplugin.h \
    ofvpntranslator.h \
    stubitem.h

SOURCES += \
    ofvpnconfiguratoradaptor.cpp \
    ofvpnplugin.cpp \
    ofvpntranslator.cpp \
    stubitem.cpp

OTHER_FILES += qmldir

MODULENAME = ru/auroraos/ofvpn
TARGETPATH = /usr/share/ru.auroraos.OFVPN/lib/qmlplugins/$$MODULENAME
DEFINES += OFVPN_PATH=\\\"$$TARGETPATH\\\"

import.files = qmldir
import.path = $$TARGETPATH
target.path = $$TARGETPATH

INSTALLS += target import
