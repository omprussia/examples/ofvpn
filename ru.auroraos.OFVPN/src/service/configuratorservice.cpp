// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "configuratorservice.h"
#include "ofvpnconfiguratordbusobject.h"

#include <QtDBus>
#include <QDir>

const QString ConfiguratorService::fncDBusService = "ru.auroraos.OFVPN";
const QString ConfiguratorService::fncDBusObject = "/ru/auroraos/RunExternalConfigurator";

const QString ConfiguratorService::serviceFileName = "ru.auroraos.OFVPN.service";
const QString ConfiguratorService::serviceTemplateFileName = serviceFileName + ".template";
const QString ConfiguratorService::serviceTemplateFilePath = QString("%1/.service/%2").arg(SHARE_DIR).arg(serviceTemplateFileName);

const QString ConfiguratorService::serviceInstallHomeDir = QString("%1/.local/share/dbus-1/services").arg(QDir::homePath());
const QString ConfiguratorService::serviceInstallFilePath = QString("%1/%2").arg(serviceInstallHomeDir).arg(serviceFileName);

/*!
 * \brief Constructor of the ConfiguratorService class.
 * \param parent Ancestor object.
 */
ConfiguratorService::ConfiguratorService(QObject *parent) : QObject(parent)
{
}

/*!
 * \brief Registers DBus service.
 */
void ConfiguratorService::registerOFVPNConfiguratorDBusService()
{
    if (isOFVPNConfiguratorDBusServiceRegistered()) {
        qInfo() << "DBus-service ru.auroraos.OFVPN already registered";
        return;
    }
    configureServiceIfNotRegistered();
    m_dbusAdaptor = new OFVPNConfiguratorAdaptor(&m_dbusObject);
    if (!QDBusConnection::sessionBus().registerService(fncDBusService)) {
        qInfo() << "Register DBus-service ru.auroraos.OFVPN";
        if (QDBusConnection::sessionBus().lastError().type() != QDBusError::NoError)
            qFatal("%s", QDBusConnection::sessionBus().lastError().message().toUtf8().data());
    }
    if (!QDBusConnection::sessionBus().registerObject(fncDBusObject, &m_dbusObject)) {
        qInfo() << "Register DBus-object ru.auroraos.OFVPN";
        if (QDBusConnection::sessionBus().lastError().type() != QDBusError::NoError)
            qFatal("%s", QDBusConnection::sessionBus().lastError().message().toUtf8().data());
    }
}

/*!
 * \brief Pass the path to the adapter to prepare the delayed response.
 */
void ConfiguratorService::sendConfigurationData(QString path)
{
    m_dbusAdaptor->sendConfigurationData(path);
}

/*!
 * \brief Checks if a service with the given name is registered.
 * \return Returns true if registered, otherwise false.
 */
bool ConfiguratorService::isOFVPNConfiguratorDBusServiceRegistered()
{
    return QDBusConnection::sessionBus().interface()->isServiceRegistered(fncDBusService);
}

/*!
 * \brief Places the service file in the user's directory.
 */
void ConfiguratorService::configureServiceIfNotRegistered()
{
    if (!QDir().exists(serviceInstallHomeDir))
        QDir().mkpath(serviceInstallHomeDir);
    if (!QFile::exists(serviceInstallFilePath))
        QFile::copy(serviceTemplateFilePath, serviceInstallFilePath);
}
