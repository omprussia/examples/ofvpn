// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef OFVPNCONFIGURATORADAPTOR_H
#define OFVPNCONFIGURATORADAPTOR_H

#include <QDBusInterface>
#include <QDBusPendingCallWatcher>
#include <QObject>

/*!
 * \brief The OFVPNConfiguratorAdaptor class
 */
class OFVPNConfiguratorAdaptor : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString vpnConfigurationData READ vpnConfigurationData WRITE setVpnConfigurationData
                       NOTIFY vpnConfigurationDataChanged)

public:
    explicit OFVPNConfiguratorAdaptor(QObject *parent = nullptr);

    Q_INVOKABLE void getConfigurationData();
    void setVpnConfigurationData(const QString path);

public slots:
    QString vpnConfigurationData();

signals:
    void vpnConfigurationDataChanged();
    void configurationReceived();

private:
    QString m_vpnConfigurationData;
    QDBusInterface *m_externalConfigurator;
    QDBusPendingCallWatcher *m_dbusWatcher;

    bool connectToConfiguratorService();
};

#endif // OFVPNCONFIGURATORADAPTOR_H
