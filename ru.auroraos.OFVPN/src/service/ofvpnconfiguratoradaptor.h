// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef OFVPNCONFIGURATORADAPTOR_H
#define OFVPNCONFIGURATORADAPTOR_H

#include <QtDBus/QDBusAbstractAdaptor>
#include <QtDBus/QDBusInterface>

/*!
 * \brief The OFVPNConfiguratorAdaptor class
 */
class OFVPNConfiguratorAdaptor : public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "ru.auroraos.RunExternalConfigurator")

public:
    explicit OFVPNConfiguratorAdaptor(QObject *parent = nullptr);
    virtual ~OFVPNConfiguratorAdaptor() override;

public slots:
    QString processConfigurationRequest(const QDBusMessage &message);
    void sendConfigurationData(QString path);

private:
    QList<QDBusMessage> m_requestDataList;
};

#endif // OFVPNCONFIGURATORADAPTOR_H
