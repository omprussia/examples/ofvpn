// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0
import ru.auroraos.ofvpn.Configurator 1.0

Dialog {
    id: root

    property string selectedConfig

    function handleDialog(path) {
        configuratorService.sendConfigurationData(path);
        Qt.quit()
    }

    canAccept: selectedConfig.length > 0

    onAccepted: handleDialog(selectedConfig)
    onRejected: handleDialog("")
    onStatusChanged: {
        if (status == DialogStatus.Opened)
            configurationsModel.updateData();
    }

    SilicaFlickable {
        objectName: "flickableContent"
        anchors.fill: parent
        contentHeight: Math.max(content.height, height)

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }

        PullDownMenu {
            objectName: "pullDownMenu"

            MenuItem {
                objectName: "editMenuItem"
                //% "Edit mode"
                text: qsTrId("config_selection_dialog-edit_mode")
                onClicked: pageStack.push(Qt.resolvedUrl("MainPage.qml"))
            }
        }
    }

    SilicaListView {
        id: contentList

        objectName: "contentList"
        anchors.fill: parent
        highlight: highlight
        highlightFollowsCurrentItem: false
        focus: true
        header: DialogHeader {
            id: header

            objectName: "header"
            //% "Select a configuration"
            title: qsTrId("config_selection_dialog-header")
            //% "Accept"
            acceptText: qsTrId("conf_button-accept")
            //% "Cancel"
            defaultCancelText: qsTrId("dialog-cancel")
        }

        model: ConfigFilesListModel {
            id: configurationsModel

            objectName: "configurationsModel"
        }

        delegate: ListItem {
            id: listItem

            objectName: "listItem"
            contentHeight: Theme.itemSizeMedium

            onClicked: {
                contentList.currentIndex = index;
                root.selectedConfig = configurationsModel.getFilePath(index);
            }

            Label {
                objectName: "configLabel"
                anchors.fill: parent
                anchors { leftMargin: Theme.horizontalPageMargin; rightMargin: anchors.leftMargin }
                verticalAlignment: Text.AlignVCenter
                text: name
            }
        }
    }

    Component {
        id: highlight

        Rectangle {
            objectName: "highlightRectangle"
            width: contentList.width
            height: Theme.itemSizeSmall
            color: Theme.highlightBackgroundColor
            opacity: Theme.highlightBackgroundOpacity
            radius: 5
            visible: selectedConfig.length > 0
            y: contentList.currentItem.y

            Behavior on y {
                objectName: "animation"

                SpringAnimation {
                    objectName: "springAnimation"
                    spring: 3
                    damping: 0.2
                }
            }
        }
    }
}
