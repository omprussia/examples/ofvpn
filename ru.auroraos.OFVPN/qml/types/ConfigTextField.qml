// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0

TextField {
    id: root

    property bool isValid: acceptableInput
    property Item nextFocusItem

    objectName: "textField"
    width: parent.width
    inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
    placeholderText: label
    color: acceptableInput ? (isValid ? Theme.primaryColor : Theme.errorColor) : Theme.errorColor

    EnterKey.iconSource: "image://theme/icon-m-enter-" + (nextFocusItem ? "next" : "close")
    EnterKey.onClicked: {
        if (nextFocusItem)
            nextFocusItem.focus = true;
        else
            focus = false;
    }
}
