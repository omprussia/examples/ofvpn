// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    objectName: "cover"

    Label {
        id: label

        objectName: "label"
        anchors.centerIn: parent

        //% "OFVPN"
        text: qsTrId("cover-ofvpn")
    }

    CoverActionList {
        id: coverAction

        objectName: "coverAction"

        CoverAction {
            objectName: "nextAction"
            iconSource: "image://theme/icon-cover-next"
        }

        CoverAction {
            objectName: "pauseAction"
            iconSource: "image://theme/icon-cover-pause"
        }
    }
}
