%global __provides_exclude_from ^%{_datadir}/%{name}.*$
%global __requires_exclude_from ^%{_datadir}/%{name}.*$

Name: ru.auroraos.OFVPN
Summary: An application to configure and manage confiugrations for OFVPN
Version: 0.1
Release: 1
License: BSD-3-Clause
URL:     https://developer.auroraos.ru/open-source
Source0: %{name}-%{version}.tar.bz2
Requires: sailfishsilica-qt5 >= 0.10.9
BuildRequires: pkgconfig(auroraapp) 
BuildRequires: pkgconfig(Qt5Core)
BuildRequires: pkgconfig(Qt5Qml)
BuildRequires: pkgconfig(Qt5Quick)
BuildRequires: pkgconfig(Qt5DBus)
BuildRequires: pkgconfig(connman)
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(dbus-1)

%description
The GUI application to create, edit and remove configurations for openfortivpn
console application that manages the VPN connection.

The application also provides data about connection to the jolla-settings

%prep
%autosetup

%build

%qmake5
%make_build

%install
%make_install

%files
%defattr(-,root,root,-)
%{_bindir}/%{name}
%defattr(644,root,root,-)
%{_datadir}/%{name}
%{_datadir}/%{name}/translations
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png

%defattr(-,root,root,-)
%{_sysconfdir}/connman/vpn-plugin/*.conf
%{_datadir}/sailfish-vpn/*
