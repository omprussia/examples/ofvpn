// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "ofvpnplugin.h"
#include "ofvpntranslator.h"
#include "stubitem.h"
#include "ofvpnconfiguratoradaptor.h"

/*!
 * \brief Sets translations for plugin fields.
 */
void OFVPNPlugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
    Q_ASSERT(uri == QLatin1String("ru.auroraos.ofvpn"));
    qInfo("Was here");

    // Always load default translations first, otherwise we'll see
    // only string IDs when running jolla-settings from terminal
    OFVPNTranslator *defaultTranslator = new OFVPNTranslator(engine);
    defaultTranslator->load(QLocale(), "ofvpn-networking-vpn-plugin", "-",
                            QString(OFVPN_PATH) + "/translations");
    qInfo("Loading first");

    OFVPNTranslator *translator = new OFVPNTranslator(engine);
    translator->load(QLocale(), "ofvpn-networking-vpn-plugin", "-",
                     QString(OFVPN_PATH) + "/translations");
    qInfo("Loading second");
    engine->installEventFilter(translator);
    qInfo("Load completed");
}

/*!
 * \brief Registers new types.
 */
void OFVPNPlugin::registerTypes(const char *uri)
{
    Q_ASSERT(uri == QLatin1String("ru.auroraos.ofvpn"));
    qInfo("Register types");
    qmlRegisterType<StubItem>("ru.auroraos.ofvpn", 1, 0, "StubItem");
    qmlRegisterType<OFVPNConfiguratorAdaptor>("ru.auroraos.ofvpn", 1, 0,
                                              "OFVPNConfiguratorAdaptor");
}
