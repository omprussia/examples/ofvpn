# OFVPN клиент

Проект-пример, показывающий как интегрировать VPN в Аврору ОС, 
который предоставляется приложением openfortivpn.

Главная цель - демонстрация с использованием минимума исходного кода 
для получения корректного VPN-плагина с внешним конфигурационным файлом.

Подпись приложения выполняется **extended**-ключами.

## Ограничения

+ CLI передача пароля, связанного с [NetworkManager-fortisslvpn#12](https://gitlab.gnome.org/GNOME/NetworkManager-fortisslvpn/issues/12)
+ Поскольку openfortivpn не имеет асинхронного интерфейса управления (аналогичного OpenVPN), в openfortivpn временно 
добавлена опция пропуска проверки сертификатов.

## Зависимости

+ connman
+ connman-vpn-scripts
+ glib-2.0
+ dbus-1
+ форк [openfortivpn](https://github.com/sailfishos-mirror/openfortivpn/tree/master)
+ jolla-settings в качестве клиента графического интерфейса

## Условия использования и участия

Исходный код проекта предоставляется по [лицензии](LICENSE.BSD-3-Clause.md),
которая позволяет использовать его в сторонних приложениях.

[Соглашение участника](CONTRIBUTING.md) регламентирует права,
предоставляемые участниками компании «Открытая Мобильная Платформа».

Информация об участниках указана в файле [AUTHORS](AUTHORS.md).

[Кодекс поведения](CODE_OF_CONDUCT.md) — это действующий набор правил
компании «Открытая Мобильная Платформа»,
который информирует об ожиданиях по взаимодействию между членами сообщества при общении и работе над проектами.

## Структура проекта

* Каталог **[ru.auroraos.OFVPN](ru.auroraos.OFVPN)** содержит внешний конфигуратор OFVPN:
    * Файл **[ru.auroraos.OFVPN.pro](ru.auroraos.OFVPN/ru.auroraos.OFVPN.pro)** 
        описывает структуру подпроекта внешнего конфигуратора для системы сборки qmake.
    * Каталог **[icons](ru.auroraos.OFVPN/icons)** содержит иконки приложения для поддерживаемых разрешений экрана.
    * Каталог **[qml](ru.auroraos.OFVPN/qml)** содержит исходный код на QML и ресурсы интерфейса пользователя.
       * Каталог **[cover](ru.auroraos.OFVPN/qml/cover)** содержит реализации обложек приложения.
       * Каталог **[pages](ru.auroraos.OFVPN/qml/pages)** содержит страницы приложения.
       * Каталог **[types](ru.auroraos.OFVPN)** содержит пользовательские QML-компоненты, которые реализуют поля ввода данных.
       * Файл **[ru.auroraos.OFVPN.qml](ru.auroraos.OFVPN/qml/ru.auroraos.OFVPN.qml)**
         предоставляет реализацию окна приложения.
    * Каталог **[src](ru.auroraos.OFVPN/src)** содержит исходный код на C++.
       * Файл **[main.cpp](ru.auroraos.OFVPN/src/main.cpp)** является точкой входа в приложение.
    * Каталог **[translations](ru.auroraos.OFVPN/translations)** содержит файлы перевода интерфейса пользователя.
    * Файл **[ru.auroraos.OFVPN.desktop](ru.auroraos.OFVPN.desktop)**
        определяет отображение и параметры запуска приложения.
* Файл **[ru.auroraos.OFVPN.pro](ru.auroraos.OFVPN.pro)** описывает
    структуру проекта для системы сборки qmake.
* Каталог **[rpm](rpm)** содержит настройки сборки rpm-пакета.
   * Файл **[ru.auroraos.OFVPN.spec](rpm/ru.auroraos.OFVPN.spec)**
             используется инструментом rpmbuild.
* Каталог **[jolla-settings-controls](jolla-settings-controls)** содержит QML-файлы управления настройками
    для Авроры ОС 3 и Авроры ОС 4.
   * Каталог **[jolla-settings-controls/aurora_4_0](jolla-settings-controls/aurora_4_0/ofvpn)** содержит QML-файлы
  модуля *jolla-setting* sailfish-vpn для Авроры ОС 4.
   * Каталог **[jolla-settings-controls/aurora_3_5](jolla-settings-controls/aurora_3_5/ofvpn)** содержит QML-файлы
  модуля *jolla-setting* sailfish-vpn для Авроры ОС 3.
* Каталог **[jolla-settings-translations](jolla-settings-translations)** содержит файлы перевода плагина OFVPN.
* Каталог **[connman-plugin](connman-plugin)** содержит подпроект с обёрткой ConnMan плагина.
   * Файл **[aurora-ofvpn-vpn.c](connman-plugin/src/aurora-ofvpn-vpn.c)** является главным файлом с макросом `CONNMAN_PLUGIN_DEFINE`.
   * Файл **[clang-format](connman-plugin/src/.clang-format)** - содержит настройки clang-format из ядра Linux.
* Каталог **[qml-plugin](qml-plugin)** содержит реализацию OFVPN плагина.

## Совместимость

Проект совместим с актуальными версиями ОС Аврора.

## Снимки экранов

![screenshots](screenshots/screenshots.png)

## This document in English

- [README.md](README.md)
