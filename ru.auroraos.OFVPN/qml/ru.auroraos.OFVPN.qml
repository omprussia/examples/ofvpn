// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0
import ru.auroraos.ofvpn.Configurator 1.0
import "pages"

ApplicationWindow
{
    id: root

    function openConfigSelectionDialog(isSelectionMode) {
        if (isSelectionMode)
            var configSelect = pageStack.push(Qt.resolvedUrl("pages/ConfigSelectionDialog.qml"));
    }

    objectName: "root"
    initialPage: Qt.resolvedUrl("pages/MainPage.qml")
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations

    Component.onCompleted: configuratorService.registerOFVPNConfiguratorDBusService()

    ConfiguratorService {
        id: configuratorService

        objectName: "configuratorService"
    }
}
