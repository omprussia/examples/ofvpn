// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef CONFIGURATORSERVICE_H
#define CONFIGURATORSERVICE_H

#include <QObject>

#include "ofvpnconfiguratordbusobject.h"
#include "ofvpnconfiguratoradaptor.h"

/*!
 * \brief The ConfiguratorService class
 */
class ConfiguratorService : public QObject
{
    Q_OBJECT

public:
    explicit ConfiguratorService(QObject *parent = nullptr);

    Q_INVOKABLE void registerOFVPNConfiguratorDBusService();
    Q_INVOKABLE void sendConfigurationData(QString path);

private:
    static const QString fncDBusService;
    static const QString fncDBusObject;

    static const QString serviceFileName;
    static const QString serviceTemplateFileName;
    static const QString serviceTemplateFilePath;
    static const QString serviceInstallHomeDir;
    static const QString serviceInstallFilePath;

    OFVPNConfiguratorDBusObject m_dbusObject;
    OFVPNConfiguratorAdaptor *m_dbusAdaptor;

    bool isOFVPNConfiguratorDBusServiceRegistered();
    void configureServiceIfNotRegistered();
};

#endif // CONFIGURATORSERVICE_H
