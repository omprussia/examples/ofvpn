<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name></name>
    <message id="ofvpn-connection">
        <source>Connection</source>
        <translation>Connection</translation>
    </message>
    <message id="ofvpn-set_dns">
        <source>Add VPN nameservers in /etc/resolv.conf when tunnel is up</source>
        <translation>Add VPN nameservers in /etc/resolv.conf when tunnel is up</translation>
    </message>
    <message id="ofvpn-try_to_configure_ip_routes">
        <source>Set if openfortivpn should try to configure IP routes through the VPN when tunnel is up</source>
        <translation>Set if openfortivpn should try to configure IP routes through the VPN when tunnel is up</translation>
    </message>
    <message id="ofvpn-half_internet_routs">
        <source>Add 0.0.0.0/1 and 128.0.0.0/1 routes with higher priority</source>
        <translation>Add 0.0.0.0/1 and 128.0.0.0/1 routes with higher priority</translation>
    </message>
    <message id="ofvpn-certificates">
        <source>Certificates</source>
        <translation>Certificates</translation>
    </message>
    <message id="ofvpn-allow_self_signed_certificate">
        <source>Allow self signed certificate</source>
        <translation>Allow self signed certificate</translation>
    </message>
    <message id="ofvpn-trusted_cert">
        <source>Trusted certificate</source>
        <translation>Trusted certificate</translation>
    </message>
    <message id="ofvpn-user_certificate">
        <source>User certificate</source>
        <translation>User certificate</translation>
    </message>
    <message id="ofvpn-user_key">
        <source>User key</source>
        <translation>User key</translation>
    </message>
    <message id="ofvpn-ca_file">
        <source>CA file</source>
        <translation>CA file</translation>
    </message>
    <message id="ofvpn-insecure_ssl">
        <source>Do not disable insecure SSL protocols/ciphers</source>
        <translation>Do not disable insecure SSL protocols/ciphers</translation>
    </message>
    <message id="config_dialog_header-parametrs">
        <source>Parametrs</source>
        <translation>Parametrs</translation>
    </message>
    <message id="conf_button-add">
        <source>Add</source>
        <translation>Add</translation>
    </message>
    <message id="conf_button-edit">
        <source>Edit</source>
        <translation>Edit</translation>
    </message>
    <message id="ofvpn-сonfiguration_name">
        <source>Configuration name</source>
        <translation>Configuration name</translation>
    </message>
    <message id="main_page-configurations">
        <source>Configurations</source>
        <translation>Configurations</translation>
    </message>
    <message id="main_page-add_configuration">
        <source>Add configuration</source>
        <translation>Add configuration</translation>
    </message>
    <message id="main_page-edit">
        <source>Edit</source>
        <translation>Edit</translation>
    </message>
    <message id="main_page-remove">
        <source>Remove</source>
        <translation>Remove</translation>
    </message>
    <message id="cover-ofvpn">
        <source>OFVPN</source>
        <translation>OFVPN</translation>
    </message>
    <message id="dialog-cancel">
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message id="config_selection_dialog-edit_mode">
        <source>Edit mode</source>
        <translation>Edit mode</translation>
    </message>
    <message id="ofvpn-name_in_use">
        <source>This name is already in use</source>
        <translation>This name is already in use</translation>
    </message>
    <message id="config_selection_dialog-header">
        <source>Select a configuration</source>
        <translation>Select a configuration</translation>
    </message>
    <message id="conf_button-accept">
        <source>Accept</source>
        <translation>Accept</translation>
    </message>
</context>
</TS>
