// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef OFVPNCONFIGURATORDBUSOBJECT_H
#define OFVPNCONFIGURATORDBUSOBJECT_H

#include <QtDBus/QDBusContext>

/*!
 * \brief OFVPNConfiguratorDBusObject class represents the DBus-service object.
 * The class inherits from QObject in order to be able to use instance of this class
 * in the class described the DBus-interface. And the class inherits from QDBusContext
 * in order to be able to send the error reply from the DBus-interface.
 * @see http://doc.qt.io/qt-5/qdbuscontext.html#details
 */
class OFVPNConfiguratorDBusObject : public QObject, public QDBusContext
{
    Q_OBJECT
};

#endif // OFVPNCONFIGURATORDBUSOBJECT_H
