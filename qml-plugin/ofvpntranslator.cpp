// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "ofvpntranslator.h"

/*!
 * \brief Installs a translator for the application.
 */
OFVPNTranslator::OFVPNTranslator(QObject *parent) : QTranslator(parent)
{
    qApp->installTranslator(this);
}

/*!
 * \brief Removes the translator of the application.
 */
OFVPNTranslator::~OFVPNTranslator()
{
    qApp->removeTranslator(this);
}

/*!
 * \brief Loads the content of translations.
 */
bool OFVPNTranslator::eventFilter(QObject *watched, QEvent *event)
{
    qInfo("event?");
    if (watched == this && event->type() == QEvent::LanguageChange)
        load(QLocale(), "ofvpn", "-", QString(OFVPN_PATH) + "/translations");

    return QTranslator::eventFilter(watched, event);
}
