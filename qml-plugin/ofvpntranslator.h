// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef OFVPNTRANSLATOR_H
#define OFVPNTRANSLATOR_H

#include <QtQml>

/*!
 * \brief The OFVPNTranslator class
 */
class OFVPNTranslator : public QTranslator
{
    Q_OBJECT

public:
    OFVPNTranslator(QObject *parent = nullptr);
    virtual ~OFVPNTranslator() override;

    bool eventFilter(QObject *watched, QEvent *event) override;
};

#endif // OFVPNTRANSLATOR_H
