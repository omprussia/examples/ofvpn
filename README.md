# OFVPN client

An example project that shows how to integrate VPN into Aurora OS which is provided by openfortivpn application.

The main purpose is to demonstrate using a minimum of source code to get a correct VPN plugin
with an external configuration file.

The application signing must be made with **extended**-keys.

## Limitations

+ CLI passing of the password, related: [NetworkManager-fortisslvpn#12](https://gitlab.gnome.org/GNOME/NetworkManager-fortisslvpn/issues/12)
+ Since openfortivpn has no async control interface (similar to OpenVPN), option skip cert check was temporary added to openfortivpn

## Dependencies

+ connman
+ connman-vpn-scripts
+ glib-2.0
+ dbus-1
+ [openfortivpn](https://github.com/sailfishos-mirror/openfortivpn/tree/master) fork
+ jolla-settings as a gui client 4

## Terms of Use and Participation

The source code of the project is provided under [the license](LICENSE.BSD-3-Clause.md),
which allows its use in third-party applications.

The [contributor agreement](CONTRIBUTING.md) documents the rights granted by contributors
of the Open Mobile Platform.

Information about the contributors is specified in the [AUTHORS](AUTHORS.md) file.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules of the Open Mobile
Platform which informs you how we expect the members of the community will interact
while contributing and communicating.

## Project Structure

**[ru.auroraos.OFVPN](ru.auroraos.OFVPN)** directory contains OFVPN external configurator:
 * **[ru.auroraos.OFVPN.pro](ru.auroraos.OFVPN/ru.auroraos.OFVPN.pro)** file 
   describes the app subproject structure for the qmake build system.
 * **[icons](ru.auroraos.OFVPN/icons)** directory contains the application icons for different screen resolutions.
 * **[qml](ru.auroraos.OFVPN/qml)** directory contains the QML source code and the UI resources.
   * **[cover](ru.auroraos.OFVPN/qml/cover)** directory contains the application cover implementations.
   * **[pages](ru.auroraos.OFVPN/qml/pages)** directory contains the application pages.
   * **[types](ru.auroraos.OFVPN)** directory contains user QML components which implements some input fields.
   * **[ru.auroraos.OFVPN.qml](ru.auroraos.OFVPN/qml/ru.auroraos.OFVPN.qml)** file
     provides the application window implementation.
 * **[src](ru.auroraos.OFVPN/src)** directory contains the C++ source code.
   * **[main.cpp](ru.auroraos.OFVPN/src/main.cpp)** file is the application entry point.
 * **[translations](ru.auroraos.OFVPN/translations)** directory contains the UI translation files.
 * **[ru.auroraos.OFVPN.desktop](ru.auroraos.OFVPN.desktop)** file
   defines the display and parameters for launching the application.
* **[ru.auroraos.OFVPN.pro](ru.auroraos.OFVPN.pro)** file
 describes the project structure for the qmake build system.
* **[rpm](rpm)** directory contains the rpm-package build settings.
  * **[ru.auroraos.OFVPN.spec](rpm/ru.auroraos.OFVPN.spec)** file is used by rpmbuild tool.
* **[jolla-settings-controls](jolla-settings-controls)** directory contains QML settings controls files for Aurora OS 3 and Aurora OS 4.
  * **[jolla-settings-controls/aurora_4_0](jolla-settings-controls/aurora_4_0/ofvpn)** directory contains QML files for
  the *jolla-setting* sailfish-vpn module for Aurora OS 4.
  * **[jolla-settings-controls/aurora_3_5](jolla-settings-controls/aurora_3_5/ofvpn)** directory contains QML files for
  the *jolla-setting* sailfish-vpn module for Aurora OS 3.
* **[jolla-settings-translations](jolla-settings-translations)** directory contains the OFVPN plugin translation files.
* **[connman-plugin](connman-plugin)** directory contains subproject with a wrapper of the ConnMan plugin.
  * **[aurora-ofvpn-vpn.c](connman-plugin/src/aurora-ofvpn-vpn.c)** is the main file with the `CONNMAN_PLUGIN_DEFINE` macros.
  * **[clang-format](connman-plugin/src/.clang-format)** file is a clang-format configuration file from Linux kernel.
* **[qml-plugin](qml-plugin)** directory contains the implementation of the OFVPN plugin.

## Compatibility

The project is compatible with all the current versions of the Aurora OS.

## Screenshots

![screenshots](screenshots/screenshots.png)

## This document in Russian / Перевод этого документа на русский язык

- [README.ru.md](README.ru.md)
