<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name></name>
    <message id="ofvpn-authentication">
        <source>Authentication</source>
        <translation>Аутентификация</translation>
    </message>
    <message id="ofvpn-server_port">
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message id="ofvpn-user_name">
        <source>Username</source>
        <translation>Имя пользователя</translation>
    </message>
    <message id="ofvpn-type_name">
        <source>OFVPN</source>
        <translation>OFVPN</translation>
    </message>
    <message id="ofvpn-type_description">
        <source>Full featured implementation of the Fortinet PPP+SSL VPN solution</source>
        <translation>Полнофункциональная реализация решения Fortinet PPP+SSL VPN</translation>
    </message>
    <message id="ofvpn-two_factor_authentication">
        <source>Two-factor authentication</source>
        <translation>Двухфакторная аутентификация</translation>
    </message>
    <message id="settings_network-he-vpn_add_new_ofvpn">
        <source>Add new OFVPN connection</source>
        <translation>Создать новое OFVPN соединение</translation>
    </message>
    <message id="settings_network-he-vpn_edit_ofvpn">
        <source>Edit OFVPN connection</source>
        <translation>Изменить OFVPN соединение</translation>
    </message>
    <message id="ofvpn_port_error">
        <source>Port must be a value between 1 and 65535</source>
        <translation>Порт должен иметь значение от 1 до 65535</translation>
    </message>
    <message id="ofvpn-config_button">
        <source>Select</source>
        <translation>Выбрать</translation>
    </message>
    <message id="ofvpn-config_header">
        <source>Config file</source>
        <translation>Конфигурационный файл</translation>
    </message>
    <message id="ofvpn-connection">
        <source>Connection</source>
        <translation>Соединение</translation>
    </message>
    <message id="ofvpn-set_dns">
        <source>Add VPN nameservers in /etc/resolv.conf when tunnel is up</source>
        <translation>Добавить серверы имен VPN в /etc/resolv.conf, когда туннель запущен</translation>
    </message>
    <message id="ofvpn-try_to_configure_ip_routes">
        <source>Set if openfortivpn should try to configure IP routes through the VPN when tunnel is up</source>
        <translation>Попытаться настроить IP-маршруты через VPN, когда туннель запущен</translation>
    </message>
    <message id="ofvpn-half_internet_routs">
        <source>Add 0.0.0.0/1 and 128.0.0.0/1 routes with higher priority</source>
        <translation>Добавить маршруты 0.0.0.0/1 и 128.0.0.0/1 с более высоким приоритетом</translation>
    </message>
    <message id="ofvpn-certificates">
        <source>Certificates</source>
        <translation>Сертификаты</translation>
    </message>
    <message id="ofvpn-allow_self_signed_certificate">
        <source>Allow self signed certificate</source>
        <translation>Разрешить самоподписанный сертификат</translation>
    </message>
    <message id="ofvpn-trusted_cert">
        <source>Trusted certificate</source>
        <translation>Доверенный сертификат</translation>
    </message>
    <message id="ofvpn-user_certificate">
        <source>User certificate</source>
        <translation>Сертификат пользователя</translation>
    </message>
    <message id="ofvpn-user_key">
        <source>User key</source>
        <translation>Ключ пользователя</translation>
    </message>
    <message id="ofvpn-ca_file">
        <source>CA file</source>
        <translation>CA файл</translation>
    </message>
    <message id="ofvpn-insecure_ssl">
        <source>Do not disable insecure SSL protocols/ciphers</source>
        <translation>Не отключать небезопасные протоколы/шифры SSL</translation>
    </message>
    <message id="config_dialog_header-parametrs">
        <source>Parametrs</source>
        <translation>Параметры</translation>
    </message>
    <message id="conf_button-add">
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message id="conf_button-edit">
        <source>Edit</source>
        <translation>Изменить</translation>
    </message>
    <message id="ofvpn-сonfiguration_name">
        <source>Configuration name</source>
        <translation>Имя конфигурации</translation>
    </message>
    <message id="main_page-configurations">
        <source>Configurations</source>
        <translation>Конфигурации</translation>
    </message>
    <message id="main_page-add_configuration">
        <source>Add configuration</source>
        <translation>Добавить конфигурацию</translation>
    </message>
    <message id="main_page-edit">
        <source>Edit</source>
        <translation>Изменить</translation>
    </message>
    <message id="main_page-remove">
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message id="dialog-cancel">
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message id="config_selection_dialog-edit_mode">
        <source>Edit mode</source>
        <translation>Режим редактирования</translation>
    </message>
    <message id="ofvpn-name_in_use">
        <source>This name is already in use</source>
        <translation>Это имя уже используется</translation>
    </message>
    <message id="config_selection_dialog-header">
        <source>Select a configuration</source>
        <translation>Выберите конфигурацию</translation>
    </message>
    <message id="conf_button-accept">
        <source>Accept</source>
        <translation>Принять</translation>
    </message>
    <message id="cover-ofvpn">
        <source>OFVPN</source>
        <translation>OFVPN</translation>
    </message>
</context>
</TS>
