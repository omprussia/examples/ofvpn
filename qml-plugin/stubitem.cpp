// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "stubitem.h"

/*!
 * \brief Constructor of the StubItem class.
 * \param parent Ancestor object.
 */
StubItem::StubItem(QObject *parent) : QObject(parent) { }
