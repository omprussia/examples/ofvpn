// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "ofvpnconfiguratoradaptor.h"

/*!
 * \brief Constructor of the OFVPNConfiguratorAdaptor class.
 * \param parent Ancestor object.
 */
OFVPNConfiguratorAdaptor::OFVPNConfiguratorAdaptor(QObject *parent)
    : QDBusAbstractAdaptor(parent)
{
}

OFVPNConfiguratorAdaptor::~OFVPNConfiguratorAdaptor()
{
    while (!m_requestDataList.isEmpty())
        sendConfigurationData(QString());
}

/*!
 * \brief Handles a request for a path to a config file.
 */
QString OFVPNConfiguratorAdaptor::processConfigurationRequest(const QDBusMessage &message)
{
    m_requestDataList.append(QDBusMessage());
    message.setDelayedReply(true);
    m_requestDataList.last() = message.createReply();

    return QString();
}

/*!
 * \brief Sends a response to the Settings application with a path to the selected configuration file.
 */
void  OFVPNConfiguratorAdaptor::sendConfigurationData(QString path)
{
    QDBusMessage reply = m_requestDataList.last();
    reply << path;
    QDBusConnection::sessionBus().send(reply);
    m_requestDataList.removeLast();
}
