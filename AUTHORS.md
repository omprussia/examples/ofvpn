# Authors

* Kirill Chuvilin, <k.chuvilin@omp.ru>
  * Product owner, 2021-2022
* Alexey Andreev <a.andreev@omp.ru>
  * Reviewer, 2022
  * Maintainer, 2022
* Andrey Vasilyev
  * Reviewer, 2021
* Nikita Medvedev
  * Developer, 2021
* Valeriya Kuchaeva
  * Icon Designer, 2022
* Vladislav Larionov
  * Reviewer, 2023
* Oksana Torosyan, <o.torosyan@omp.ru>
  * Designer, 2024
