// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <auroraapp.h>
#include <QCommandLineParser>
#include <QGuiApplication>
#include <QQuickView>
#include <QQuickItem>

#include "configfileslistmodel.h"
#include "service/configuratorservice.h"

int main(int argc, char *argv[])
{
    //! Registers ConfigFilesListModel and ConfiguratorService as QML types
    qmlRegisterType<ConfigFilesListModel>("ru.auroraos.ofvpn.Configurator", 1, 0, "ConfigFilesListModel");
    qmlRegisterType<ConfiguratorService>("ru.auroraos.ofvpn.Configurator", 1, 0, "ConfiguratorService");

    QScopedPointer<QGuiApplication> app(Aurora::Application::application(argc, argv));
    QScopedPointer<QQuickView> view(Aurora::Application::createView());
    view->setSource(Aurora::Application::pathToMainQml());

    //! Adding the --select-configuration command line option
    QCommandLineParser parser;
    parser.addOption({"select-configuration", "Сonfiguration selection mode. The configuration selection dialog box opens instead of the main page."});
    parser.process(app->arguments());

    //! Calling a QML method that defines the operating mode of an application
    bool isSelectionMode = parser.isSet("select-configuration");
    QObject *object = view->rootObject();
    QMetaObject::invokeMethod(object, "openConfigSelectionDialog", Q_ARG(QVariant, isSelectionMode));
    view->show();

    return app->exec();
}
