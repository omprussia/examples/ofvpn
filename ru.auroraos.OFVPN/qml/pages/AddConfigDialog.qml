// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0
import '../types'

Dialog {
    id: root

    property string stateType
    property string confListRegExp

    function setParametrs(paramList) {
        var getProperty = function(name) {
            var param = paramList[name];
            if (param)
                return String(param).trim();

            return '';
        }

        ofvpnConfigName.text = getProperty('configName');
        ofvpnAllowSelfSignedCert.checked = getProperty('trust-all-certs') === '1';
        ofvpnSetRoutes.checked = getProperty('set-routes') === '1';
        ofvpnSetDNS.checked = getProperty('set-dns') === '1';
        ofvpnHalfInternetRoutes.checked = getProperty('half-internet-routes') === '1';
        ofvpnInsecureSSL.checked = getProperty('insecure-ssl') === '1';
        ofvpnTrustedCert.text = getProperty('trusted-cert');
        ofvpnUserCert.selectedFilePath = getProperty('user-cert');
        ofvpnUserKey.selectedFilePath = getProperty('user-key');
        ofvpnCAfile.selectedFilePath = getProperty('ca-file');
    }

    function updateParametrs(paramList) {
        var updateProvider = function(name, value) {
            // If the value is empty, do not include the property in the configuration
            if (value !== '')
                paramList[name] = value;
            else
                delete paramList[name];
        }

        updateProvider('configName', ofvpnConfigName.text);
        updateProvider('trust-all-certs', ofvpnAllowSelfSignedCert.checked ? '1' : '0');
        updateProvider('set-routes', ofvpnSetRoutes.checked ? '1' : '0');
        updateProvider('set-dns', ofvpnSetDNS.checked ? '1' : '0');
        updateProvider('half-internet-routes', ofvpnHalfInternetRoutes.checked ? '1' : '0');
        updateProvider('insecure-ssl', ofvpnInsecureSSL.checked ? '1' : '0');
        updateProvider('trusted-cert', ofvpnTrustedCert.text);
        updateProvider('user-cert', ofvpnUserCert.selectedFilePath);
        updateProvider('user-key', ofvpnUserKey.selectedFilePath);
        updateProvider('ca-file', ofvpnCAfile.selectedFilePath);
    }

    function isAvailableInput(regExpString, text) {
        var regExVal = new RegExp(regExpString);

        return !regExVal.test(text);
    }

    objectName: "root"
    canAccept: ofvpnConfigName.isValid && ofvpnConfigName.acceptableInput

    SilicaFlickable {
        objectName: "flickableContent"
        anchors.fill: parent
        contentHeight: Math.max(content.height, height)

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }

        Column {
            id: content

            objectName: "content"
            width: parent.width

            DialogHeader {
                id: header

                objectName: "header"
                //% "Parametrs"
                title: qsTrId("config_dialog_header-parametrs")
                state: root.stateType
                //% "Cancel"
                defaultCancelText: qsTrId("dialog-cancel")

                states: [
                    State {
                        objectName: "addState"
                        name: "add"

                        PropertyChanges {
                            objectName: "addHeaderBinding"
                            target: header
                            //% "Add"
                            acceptText: qsTrId("conf_button-add")
                        }
                    },
                    State {
                        objectName: "editState"
                        name: "edit"

                        PropertyChanges {
                            objectName: "editHeaderBinding"
                            target: header
                            //% "Edit"
                            acceptText: qsTrId("conf_button-edit")
                        }
                    }
                ]
            }

            ConfigTextField {
                id: ofvpnConfigName

                objectName: "ofvpnConfigName"
                                                       //% "Configuration name"
                label: ofvpnConfigName.isValid ? qsTrId("ofvpn-сonfiguration_name")
                                                       //% "This name is already in use"
                                                     : qsTrId("ofvpn-name_in_use")
                isValid: isAvailableInput(confListRegExp, ofvpnConfigName.text)
                validator: RegExpValidator { regExp: /[^\s\/\\:\*\?"><\|]+/g }
                inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
            }

            SectionHeader {
                objectName: "connectionHeader"
                //% "Connection"
                text: qsTrId("ofvpn-connection")
            }

            TextSwitch {
                id: ofvpnSetDNS

                objectName: "ofvpnSetDNS"
                //% "Add VPN nameservers in /etc/resolv.conf when tunnel is up"
                text: qsTrId("ofvpn-set_dns")
            }

            TextSwitch {
                id: ofvpnSetRoutes

                objectName: "ofvpnSetRoutes"
                //% "Set if openfortivpn should try to configure IP routes through the VPN when tunnel is up"
                text: qsTrId("ofvpn-try_to_configure_ip_routes")
            }

            TextSwitch {
                id: ofvpnHalfInternetRoutes

                objectName: "ofvpnHalfInternetRoutes"
                //% "Add 0.0.0.0/1 and 128.0.0.0/1 routes with higher priority"
                text: qsTrId("ofvpn-half_internet_routs")
            }

            TextSwitch {
                id: ofvpnInsecureSSL

                objectName: "ofvpnInsecureSSL"
                //% "Do not disable insecure SSL protocols/ciphers"
                text: qsTrId("ofvpn-insecure_ssl")
            }

            SectionHeader {
                objectName: "certificatesHeader"
                //% "Certificates"
                text: qsTrId("ofvpn-certificates")
            }

            TextSwitch {
                id: ofvpnAllowSelfSignedCert

                objectName: "ofvpnAllowSelfSignedCert"
                //% "Allow self signed certificate"
                text: qsTrId("ofvpn-allow_self_signed_certificate")
            }

            ConfigTextField {
                id: ofvpnTrustedCert

                objectName: "ofvpnTrustedCert"
                //% "Trusted certificate"
                label: qsTrId("ofvpn-trusted_cert")
            }

            ConfigFolderPickerField {
                id: ofvpnUserCert

                objectName: "ofvpnUserCert"
                //% "User certificate"
                name: qsTrId("ofvpn-user_certificate")
            }

            ConfigFolderPickerField {
                id: ofvpnUserKey

                objectName: "ofvpnUserKey"
                //% "User key"
                name: qsTrId("ofvpn-user_key")
            }

            ConfigFolderPickerField {
                id: ofvpnCAfile

                objectName: "ofvpnCAfile"
                //% "CA file"
                name: qsTrId("ofvpn-ca_file")
            }
        }
    }
}
