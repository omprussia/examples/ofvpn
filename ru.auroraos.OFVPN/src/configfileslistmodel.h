// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef CONFIGFILESLISTMODEL_H
#define CONFIGFILESLISTMODEL_H

#include <QAbstractListModel>
#include <QFile>

/*!
 * \brief The ConfigFilesListModel class
 */
class ConfigFilesListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum ConfigRoles {
       ConfigFileNameRole = Qt::UserRole + 1
    };

    explicit ConfigFilesListModel(QObject *parent = 0);

    int rowCount(const QModelIndex&) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    virtual QHash<int, QByteArray> roleNames() const override;
    Q_INVOKABLE void addConf(QVariantMap paramList);
    Q_INVOKABLE void editConf(const int i, QVariantMap paramList);
    Q_INVOKABLE void removeConf(const int i);
    Q_INVOKABLE QVariantMap getConfigParams(const int i);
    Q_INVOKABLE QString getFilePath(const int i);
    Q_INVOKABLE QString getConfListRegExp(const int i = -1);
    Q_INVOKABLE void updateData();

private:
    QString m_appDataPath;
    QVector<QString> m_connectionsList;

    void setConfigParams(QVariantMap &paramList, QFile &file);
    QString makeFilePath(const QString name);
    QString makeFileName(const QString name);
    bool validIndex(const int index);
};

#endif // CONFIGFILESLISTMODEL_H
