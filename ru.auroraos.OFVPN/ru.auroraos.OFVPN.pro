# SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TARGET = ru.auroraos.OFVPN

CONFIG += auroraapp


QT += dbus

SHARE_DIR = /usr/share/$$TARGET

DEFINES += \
    SHARE_DIR=\\\"$$SHARE_DIR\\\"

SOURCES += \
    src/main.cpp \
    src/service/configuratorservice.cpp \
    src/configfileslistmodel.cpp \
    src/service/ofvpnconfiguratoradaptor.cpp

DISTFILES += \
    ru.auroraos.OFVPN.desktop \
    qml/ru.auroraos.OFVPN.qml \
    qml/cover/CoverPage.qml \
    qml/pages/AddConfigDialog.qml \
    qml/pages/ConfigSelectionDialog.qml \
    qml/pages/MainPage.qml \
    qml/types/ConfigFolderPickerField.qml \
    qml/types/ConfigPasswordField.qml \
    qml/types/ConfigTextField.qml \
    ru.auroraos.OFVPN.service.template

AURORAAPP_ICONS = 86x86 108x108 128x128 172x172

CONFIG += auroraapp_i18n

HEADERS += \
    src/configfileslistmodel.h \
    src/service/configuratorservice.h \
    src/service/ofvpnconfiguratoradaptor.h \
    src/service/ofvpnconfiguratordbusobject.h

service.files += ru.auroraos.OFVPN.service.template
service.path = $$SHARE_DIR/.service

INSTALLS += service

include(translations/translations.pri)

MODULENAME = ru/auroraos/ofvpn
DEFINES += OFVPN_PATH=\\\"/usr/share/ru.auroraos.OFVPN/lib/qmlplugins/$$MODULENAME\\\"
