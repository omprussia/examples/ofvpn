// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "configfileslistmodel.h"

#include <QTextStream>
#include <QDir>
#include <QStandardPaths>

/*!
 * \brief ConfigFilesListModel constructor
 * \param parent Ancestor object.
 */
ConfigFilesListModel::ConfigFilesListModel(QObject *parent) : QAbstractListModel(parent)
{
    updateData();
}

/*!
 * \brief Returns the number of created configurations.
 */
int ConfigFilesListModel::rowCount(const QModelIndex &) const
{
    return m_connectionsList.size();
}

/*!
 * \brief Sets the data in the displayed
 * model list item depending on its role
 */
QVariant ConfigFilesListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (role == ConfigFileNameRole)
        return QVariant(m_connectionsList[index.row()].split('.')[0]);

    return QVariant();
}

/*!
 * \brief Returns the list of model roles
 */
QHash<int, QByteArray> ConfigFilesListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[ConfigFileNameRole] = "name";

    return roles;
}

/*!
 * \brief Returns a regular expression
 * matching the file name of the configuration
 */
QString ConfigFilesListModel::getConfListRegExp(const int i)
{
    if (i < -1 || i > m_connectionsList.size())
        return QString();

    QString regExp = "^(";
    QVectorIterator<QString> elem(m_connectionsList);
    while (elem.hasNext()) {
        QString fileName = elem.next().split('.')[0];
        if (i == -1 || fileName != m_connectionsList[i].split('.')[0])
            regExp += fileName + "|";
    }
    regExp += " )$";

    return regExp;
}

/*!
 * \brief Deletes an existing configuration file
 * \param i Index of the file to be deleted in the model list
 */
void ConfigFilesListModel::removeConf(const int i)
{
    if (!validIndex(i))
        return;

    beginRemoveRows(QModelIndex(), i, i);
    QFile::remove(getFilePath(i));
    m_connectionsList.erase(m_connectionsList.begin() + i);
    endRemoveRows();
}

/*!
 * \brief Changes an existing configuration file
 * \param i Index of the file to be changed in the model list
 * \param paramList List of new configuration parameters
 */
void ConfigFilesListModel::editConf(const int i, QVariantMap paramList)
{
    if (!validIndex(i))
        return;

    QFile file(getFilePath(i));
    file.rename(makeFilePath(paramList["configName"].toString()));
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    setConfigParams(paramList, file);
    file.close();
    m_connectionsList[i] = makeFileName(paramList["configName"].toString());
    emit dataChanged(index(i), index(i));
}

/*!
 * \brief Create new configuration file
 * \param paramList List of configuration parameters
 */
void ConfigFilesListModel::addConf(QVariantMap paramList)
{
    beginInsertRows(QModelIndex(), 0, 0);
    m_connectionsList.insert(0, makeFileName(paramList["configName"].toString()));
    endInsertRows();
    QFile file(makeFilePath(paramList["configName"].toString()));
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    setConfigParams(paramList, file);
    file.close();
}

/*!
 * \brief Returns the path to the configuration file
 */
QString ConfigFilesListModel::getFilePath(const int i)
{
    if (!validIndex(i))
        return QString();

    return m_appDataPath + "/" + m_connectionsList[i];
}

/*!
 * \brief Returns a list of parameters of an existing configuration file
 */
QVariantMap ConfigFilesListModel::getConfigParams(const int i)
{
    if (!validIndex(i))
        return QVariantMap();

    QVariantMap paramList;
    QFile file(getFilePath(i));
    paramList["configName"] = m_connectionsList[i].split('.')[0];
    if ((file.exists()) && (file.open(QIODevice::ReadOnly | QIODevice::Text)))
        while (!file.atEnd()) {
            QList<QByteArray> buff = file.readLine().split('=');
            paramList[buff[0]] = buff[1];
        }
    file.close();

    return paramList;
}

/*!
 * \brief Updates the model data, namely the list of configuration files
 */
void ConfigFilesListModel::updateData()
{
    beginResetModel();
    m_connectionsList.clear();
    QDir connectionsDir;
    m_appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/connections";
    connectionsDir.mkpath(m_appDataPath);
    connectionsDir.cd(m_appDataPath);
    connectionsDir.setNameFilters({"*.conf"});
    connectionsDir.setFilter(QDir::Files | QDir::NoSymLinks);
    QFileInfoList list = connectionsDir.entryInfoList();
    for (int i = 0; i < list.size(); ++i)
        m_connectionsList << QString(list.at(i).fileName());
    endResetModel();
}

/*!
 * \brief Writes parameters to the configuration file
 * \param paramList List of configuration parameters
 * \param file QFile object of the configuration file
 */
void ConfigFilesListModel::setConfigParams(QVariantMap &paramList, QFile &file)
{
    QTextStream out(&file);
    QMapIterator<QString, QVariant> elem(paramList);
    while (elem.hasNext()) {
        elem.next();
        if (elem.key() != "configName")
            out << elem.key() << "=" << elem.value().toString() << endl;
    }
}

/*!
 * \brief Returns the full path to the configuration file
 * \param name Configuration file name without an extension
 */
QString ConfigFilesListModel::makeFilePath(const QString name)
{
    return m_appDataPath + "/" + name + ".conf";
}

/*!
 * \brief Returns the name of the configuration file with an extension
 * \param name Configuration file name without an extension
 */
QString ConfigFilesListModel::makeFileName(const QString name)
{
    return name + ".conf";
}

/*!
 * \brief Verifies if the index is valid
 */
bool ConfigFilesListModel::validIndex(const int index)
{
    return index >= 0 && index <= m_connectionsList.size();
}
