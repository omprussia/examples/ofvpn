// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Settings.Networking.Vpn 1.0
import ru.auroraos.ofvpn 1.0

VpnPlatformEditDialog {
    property alias vpnConfigurationData: OFVPNConfiguratorAdaptor.vpnConfigurationData

    function basename(str) {
        return (str.slice(str.lastIndexOf("/") + 1));
    }

    //% "Add new OFVPN connection"
    title: newConnection ? qsTrId("settings_network-he-vpn_add_new_ofvpn")
                           //% "Edit OFVPN connection"
                         : qsTrId("settings_network-he-vpn_edit_ofvpn")

    objectName: "editPage"
    vpnType: "ofvpn"
    firstAdditionalItem: ofvpnPort

    Component.onCompleted: {
        init();
        ofvpnPort.text = getProviderProperty('ofvpn.Port');
        ofvpnUsername.text = getProviderProperty('ofvpn.User');
        ofvpnPassword.text = getProviderProperty('ofvpn.Password');
        ofvpnAskOTP.checked = getProviderProperty('ofvpn.AskOTP') === 'true';
        vpnConfigurationData = getProviderProperty('ofvpn.Config');
    }

    onAccepted: {
        updateProvider('ofvpn.Port', ofvpnPort.text);
        updateProvider('ofvpn.User', ofvpnUsername.text);
        updateProvider('ofvpn.Password', ofvpnPassword.text);
        updateProvider('ofvpn.AskOTP', ofvpnAskOTP.checked ? 'true' : 'false');
        updateProvider('ofvpn.Config', vpnConfigurationData);
        saveConnection();
    }

    Connections {
        target: OFVPNConfiguratorAdaptor

        onConfigurationReceived: OFVPNConfig.enabled = true
    }

    OFVPNConfiguratorAdaptor {
        id: OFVPNConfiguratorAdaptor

        objectName: "OFVPNConfiguratorAdaptor"
    }

    ConfigTextField {
        id: ofvpnPort

        objectName: "ofvpnPort"
        inputMethodHints: Qt.ImhDigitsOnly

        //% "Port"
        label: qsTrId("ofvpn-server_port")
    }

    SectionHeader {
        objectName: "configHeader"
        //% "Config file"
        text: qsTrId("ofvpn-config_header")
    }

    ValueButton {
        id: OFVPNConfig

        objectName: "configValueButton"
        //% "Select"
        label: qsTrId("ofvpn-config_button")
        value: basename(vpnConfigurationData)

        onClicked: {
            OFVPNConfig.enabled = false;
            OFVPNConfiguratorAdaptor.getConfigurationData();
        }
    }

    SectionHeader {
        objectName: "authentication"
        //% "Authentication"
        text: qsTrId("ofvpn-authentication")
    }

    ConfigTextField {
        id: ofvpnUsername

        objectName: "ofvpnUsername"
        //% "Username"
        label: qsTrId("ofvpn-user_name")
    }

    ConfigPasswordField {
        id: ofvpnPassword

        objectName: "ofvpnPassword"
    }

    TextSwitch {
        id: ofvpnAskOTP

        objectName: "ofvpnAskOTP"
        //% "Two-factor authentication"
        text: qsTrId("ofvpn-two_factor_authentication")
    }
}
