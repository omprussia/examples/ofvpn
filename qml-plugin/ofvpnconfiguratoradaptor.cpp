// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QDebug>
#include <QDBusPendingReply>

#include "ofvpnconfiguratoradaptor.h"

static const QString SERVICE_NAME = QStringLiteral("ru.auroraos.OFVPN");
static const QString OBJECT_NAME = QStringLiteral("/ru/auroraos/RunExternalConfigurator");
static const QString INTERFACE_NAME = QStringLiteral("ru.auroraos.RunExternalConfigurator");

/*!
 * \brief Constructor of the OFVPNConfiguratorAdaptor class.
 * \param parent Ancestor object.
 */
OFVPNConfiguratorAdaptor::OFVPNConfiguratorAdaptor(QObject *parent) : QObject(parent) { }

/*!
 * Returns the path to the configuration file.
 */
QString OFVPNConfiguratorAdaptor::vpnConfigurationData()
{
    return m_vpnConfigurationData;
}

/*!
 * Sets the path to the configuration file.
 */
void OFVPNConfiguratorAdaptor::setVpnConfigurationData(const QString path)
{
    m_vpnConfigurationData = path;
    emit vpnConfigurationDataChanged();
}

/*!
 * Connects to the service of the external configurator.
 */
bool OFVPNConfiguratorAdaptor::connectToConfiguratorService()
{
    m_externalConfigurator = new QDBusInterface(SERVICE_NAME, OBJECT_NAME, INTERFACE_NAME,
                                                QDBusConnection::sessionBus(), this);
    if (m_externalConfigurator == nullptr) {
        qWarning("D-Bus interface is not created");
        return false;
    }

    return true;
}

/*!
 * \brief Sends a request to get the path to the configuration file.
 * After receiving it saves the path.
 */
void OFVPNConfiguratorAdaptor::getConfigurationData()
{
    if (!connectToConfiguratorService())
        return;

    QDBusPendingCall async =
            m_externalConfigurator->asyncCall(QLatin1String("processConfigurationRequest"));
    m_dbusWatcher = new QDBusPendingCallWatcher(async, this);
    connect(m_dbusWatcher, &QDBusPendingCallWatcher::finished, [&](QDBusPendingCallWatcher *call) {
        QDBusPendingReply<QString> reply = *call;
        if (reply.isError()) {
            qWarning() << reply.error();
        } else {
            m_vpnConfigurationData = reply.argumentAt<0>();
            emit vpnConfigurationDataChanged();
            emit configurationReceived();
        }
        call->deleteLater();
    });
}
