// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0
import ru.auroraos.ofvpn.Configurator 1.0

Page {
    id: root

    objectName: "mainPage"

    SilicaFlickable {
        objectName: "flickableContent"
        anchors.fill: parent

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }

        PullDownMenu {
            objectName: "pullDownMenu"

            MenuItem {
                objectName: "addConfigurationItem"
                //% "Add configuration"
                text: qsTrId("main_page-add_configuration")

                onClicked: {
                    var paramList = {};
                    var addConfigPage = pageStack.push(Qt.resolvedUrl("AddConfigDialog.qml"));
                    addConfigPage.stateType = "add";
                    addConfigPage.confListRegExp = confModel.getConfListRegExp();
                    addConfigPage.onAccepted.connect(function() {
                        addConfigPage.updateParametrs(paramList);
                        confModel.addConf(paramList);
                    });
                }
            }
        }

        SilicaListView {
            objectName: "content"
            anchors.fill: parent

            header: PageHeader {
                id: header

                objectName: "header"
                //% "Configurations"
                title: qsTrId("main_page-configurations")
            }

            model: ConfigFilesListModel {
                id: confModel

                objectName: "confModel"
            }

            delegate: ListItem {
                id: listItem

                function edit() {
                    var paramList = confModel.getConfigParams(index);
                    var editConfigPage = pageStack.push(Qt.resolvedUrl("AddConfigDialog.qml"));
                    editConfigPage.stateType = "edit";
                    editConfigPage.confListRegExp = confModel.getConfListRegExp(index);
                    editConfigPage.setParametrs(paramList);
                    editConfigPage.onAccepted.connect(function() {
                        editConfigPage.updateParametrs(paramList);
                        confModel.editConf(index, paramList);
                    });
                }

                function remove() {
                    remorseAction("Remove", function() {
                        confModel.removeConf(index);
                    });
                }

                objectName: "listItem"
                menu: contextMenu
                contentHeight: Theme.itemSizeMedium

                ListView.onRemove: animateRemoval(listItem)

                Label {
                    objectName: "configurationLabel"
                    anchors.fill: parent
                    anchors { leftMargin: Theme.horizontalPageMargin; rightMargin: anchors.leftMargin }
                    verticalAlignment: Text.AlignVCenter
                    text: name
                }

                Component {
                    id: contextMenu

                    ContextMenu {
                        objectName: "contextMenu"

                        MenuItem {
                            objectName: "editItem"
                            //% "Edit"
                            text: qsTrId("main_page-edit")
                            onClicked: edit()
                        }
                        MenuItem {
                            objectName: "removeItem"
                            //% "Remove"
                            text: qsTrId("main_page-remove")
                            onClicked: remove()
                        }
                    }
                }
            }
        }
    }
}
