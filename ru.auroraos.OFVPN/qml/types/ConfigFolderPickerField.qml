// SPDX-FileCopyrightText: Copyright 2021 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0

Item {
    id: root

    property string selectedFile
    property string selectedFilePath
    property string name

    objectName: "root"
    height: Theme.itemSizeMedium
    anchors {
        left: parent.left
        right: parent.right
        leftMargin: Theme.horizontalPageMargin
    }

    onSelectedFilePathChanged: {
        var splitedPath = selectedFilePath.split('/');
        selectedFile = splitedPath[splitedPath.length - 1];
    }

    Label {
        id: filePath

        objectName: "filePath"
        truncationMode: TruncationMode.Fade
        text: selectedFile ? selectedFile : ""
        state: selectedFile ? "filed" : "empty"
        anchors {
            left: parent.left
            right: pickerButton.left
            top: parent.top
            bottom: parent.bottom
            topMargin: Theme.paddingMedium
            bottomMargin: Theme.paddingMedium
        }

        Label {
            id: labelText

            objectName: "labelText"
            text: root.name
            color: Theme.secondaryColor
            font.pixelSize: Theme.fontSizeSmall
            visible: selectedFile ? true : false
            anchors {
                left: parent.left
                bottom: parent.bottom
            }
        }

        MouseArea {
            id: lableClick

            objectName: "lableClick"
            anchors.fill: parent

            onClicked: {
                if (pickerButton.state === "add")
                    pageStack.push(filePickerPage);
            }
        }

        states: [
            State {
                objectName: "emptyState"
                name: "empty"

                PropertyChanges {
                    objectName: "emptyStateBinding"
                    target: filePath
                    text: root.name
                    color: Theme.secondaryColor
                }
            },
            State {
                objectName: "filedSate"
                name: "filed"

                PropertyChanges {
                    objectName: "filedStateBinding"
                    target: filePath
                    color: Theme.primaryColor
                }
            }
        ]
    }

    IconButton {
        id: pickerButton

        objectName: "pickerButton"
        state: selectedFile ? "clear" : "add"
        anchors {
            right: parent.right
            top: parent.top
            bottom: parent.bottom
        }

        onClicked: {
            if (state === "add")
                pageStack.push(filePickerPage);
            else
                root.selectedFilePath = "";
        }

        states: [
            State {
                objectName: "addState"
                name: "add"

                PropertyChanges {
                    objectName: "addStateBinding"
                    target: pickerButton
                    icon.source: "image://theme/icon-m-add?" + (pressed
                                                                ? Theme.highlightColor
                                                                : Theme.primaryColor)
                }
            },
            State {
                objectName: "clearState"
                name: "clear"

                PropertyChanges {
                    objectName: "clearStateBinding"
                    target: pickerButton
                    icon.source: "image://theme/icon-m-clear?" + (pressed
                                                                  ? Theme.highlightColor
                                                                  : Theme.primaryColor)
                }
            }
        ]
    }

    Component {
        id: filePickerPage

        FilePickerPage {
            objectName: "filePickerPage"
            nameFilters: [ '*.crt', '*.key' ]
            onSelectedContentPropertiesChanged: {
                root.selectedFilePath = selectedContentProperties.filePath;
            }
        }
    }
}
