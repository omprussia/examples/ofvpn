# SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TEMPLATE = subdirs
CONFIG += ordered

SUBDIRS = \
  ru.auroraos.OFVPN \
  connman-plugin \
  jolla-settings-controls \
  jolla-settings-translations \
  qml-plugin

OTHER_FILES += \
    rpm/ru.auroraos.OFVPN.spec \
    README.md \
    README.ru.md \
    AUTHORS.md \
    CODE_OF_CONDUCT.md \
    CONTRIBUTING.md \
    LICENSE.BSD-3-Clause.md
